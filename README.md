# Quotes - in Deno

Send a random quote to the console. 

## Run locally 

```sh
deno run src/main.js
```
## Bundle 

```sh
deno bundle src/main.js dist/quotes.js
```

## Compile as an executable 

```sh
deno compile --unstable --lite --output dist/bin/quotes src/main.js
```

## Install from the URL

```sh
deno install https://gitlab.com/coolsoftwaretyler/deno_quotes/-/raw/master/dist/quotes.js
```

## Upgrade

If you're upgrading to a new version of Quotes, you'll have to use the `--reload` flag for the installer:

```
deno install --reload -f https://gitlab.com/coolsoftwaretyler/deno_quotes/-/raw/master/dist/quotes.js
```