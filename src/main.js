const quotes = [
  `Don't get hung up on your views of how things "should" be because you will miss out on learning how they really are - Ray Dalio`,
  `You always own the option of having no opinion. There is never any need to get worked up or to trouble your soul about things you can't control. These things are not asking to be judged by you. Leave them alone. - Marcus Aurelius`,
  `If you want me to give you a two-hour presentation, I am ready today.  If you want only a five-minute speech, it will take me two weeks to prepare. — Mark Twain`
];
const random_quote = Math.floor(Math.random() * Math.floor(quotes.length));

console.log(quotes[random_quote]);
